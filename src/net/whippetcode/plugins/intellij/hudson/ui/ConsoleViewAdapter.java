/*
 * Copyright (c) 2009, David A. Freels Sr.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that
 * the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <ORGANIZATION> nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package net.whippetcode.plugins.intellij.hudson.ui;

import com.intellij.execution.filters.Filter;
import com.intellij.execution.filters.HyperlinkInfo;
import com.intellij.execution.process.ProcessHandler;
import com.intellij.execution.ui.ConsoleView;
import com.intellij.execution.ui.ConsoleViewContentType;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.wm.ToolWindow;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;

/**
 * @author David A. Freels Sr.
 */
public class ConsoleViewAdapter implements ConsoleView
{
	private ConsoleView console;
	private ToolWindow window;

	public ConsoleViewAdapter(ConsoleView console, ToolWindow window)
	{
		this.console = console;
		this.window = window;
	}

	public void print(String s, ConsoleViewContentType consoleViewContentType)
	{
		this.console.print(s, consoleViewContentType);
		if (!window.isVisible())
		{
			window.show(new Runnable() {
				public void run()
				{
				}
			});
		}
	}

	public void clear()
	{
		this.console.clear();
	}

	public void scrollTo(int i)
	{
		this.console.scrollTo(i);
	}

	public void attachToProcess(ProcessHandler processHandler)
	{
		this.console.attachToProcess(processHandler);
	}

	public void setOutputPaused(boolean b)
	{
		this.console.setOutputPaused(b);
	}

	public boolean isOutputPaused()
	{
		return this.console.isOutputPaused();
	}

	public boolean hasDeferredOutput()
	{
		return this.console.hasDeferredOutput();
	}

	public void performWhenNoDeferredOutput(Runnable runnable)
	{
		this.console.performWhenNoDeferredOutput(runnable);
	}

	public void setHelpId(String s)
	{
		this.console.setHelpId(s);
	}

	public void addMessageFilter(Filter filter)
	{
		this.console.addMessageFilter(filter);
	}

	public void printHyperlink(String s, HyperlinkInfo hyperlinkInfo)
	{
		this.console.printHyperlink(s, hyperlinkInfo);
	}

	public int getContentSize()
	{
		return this.console.getContentSize();
	}

	public boolean canPause()
	{
		return this.console.canPause();
	}

	@NotNull
	public AnAction[] createUpDownStacktraceActions()
	{
		return this.console.createUpDownStacktraceActions();
	}

	public JComponent getComponent()
	{
		return this.console.getComponent();
	}

	public JComponent getPreferredFocusableComponent()
	{
		return this.console.getPreferredFocusableComponent();
	}

	public void dispose()
	{
		this.console.dispose();
	}
}
