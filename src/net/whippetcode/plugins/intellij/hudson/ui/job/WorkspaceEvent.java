package net.whippetcode.plugins.intellij.hudson.ui.job;

import javax.swing.tree.TreePath;
import java.util.EventObject;

/**
 * @author David A. Freels Sr.
 */
public class WorkspaceEvent extends EventObject
{
	private TreePath path;

	public WorkspaceEvent(Object o)
	{
		super(o);
	}

	public TreePath getPath()
	{
		return path;
	}

	public void setPath(TreePath path)
	{
		this.path = path;
	}
}
