package net.whippetcode.plugins.intellij.hudson.ui.job;

import net.whippetcode.hudson.manager.JobManager;
import net.whippetcode.hudson.util.JavaUtil;
import net.whippetcode.plugins.intellij.hudson.domain.job.Build;
import net.whippetcode.plugins.intellij.hudson.domain.job.BuildType;
import net.whippetcode.plugins.intellij.hudson.domain.job.ChangeSetItem;
import net.whippetcode.plugins.intellij.hudson.domain.job.ChangeSetItemPath;
import net.whippetcode.plugins.intellij.hudson.domain.job.EditType;
import net.whippetcode.plugins.intellij.hudson.domain.job.Job;
import net.whippetcode.plugins.intellij.hudson.domain.job.Module;
import net.whippetcode.plugins.intellij.hudson.domain.job.ChildGroup;
import net.whippetcode.plugins.intellij.hudson.parser.sax.ColorTagCommand;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * @author David A. Freels Sr
 */
public class JobBuilder
{
	private static final ColorTagCommand COLOR_TAG_COMMAND = new ColorTagCommand();
	private static final DateFormat FORMAT = new SimpleDateFormat("MM/dd/yyyy HH:mm");

	public static List<Job> convertToPluginJob(List<net.whippetcode.hudson.domain.Job> jobs, String serverAddress)
	{
		List<Job> returnList = new ArrayList<Job>();

		for (net.whippetcode.hudson.domain.Job j : jobs)
		{
			//Populate the main job data
			Job job = new Job();
			job.setName(j.getName());
			job.setQueued(j.isQueued());
			job.setRunning(j.isRunning());
			job.setServerAddress(serverAddress);
			COLOR_TAG_COMMAND.processText(j.getColor(), job);

			//Populate the Modules first, if they exist
			if (j.getModules() != null)
			{
				ChildGroup<Module> moduleParent = new ChildGroup<Module>();
				moduleParent.setName("Modules");
				job.addChild(moduleParent);
				
				for (net.whippetcode.hudson.domain.Module m : JavaUtil.convertSeqToJList(j.getModules()))
				{
					Module module = new Module();
					module.setName(m.getDisplayName());
					module.setStatus(COLOR_TAG_COMMAND.parseStatus(m.getColor()));
					moduleParent.getChildren().add(module);
				}
			}

			//Populate the Builds
			ChildGroup<Build> buildParent = new ChildGroup<Build>();
			buildParent.setName("Builds");
			job.addChild(buildParent);
			List<net.whippetcode.hudson.domain.Build> builds = JavaUtil.convertSeqToJList(JobManager.getJobBuilds(j));
			for (net.whippetcode.hudson.domain.Build b : builds)
			{
				Build build = new Build();
				build.setDate(FORMAT.format(b.getBuildDate()));
				build.setRevision(b.getNumber());
				build.setServerName(serverAddress);
				build.setType(BuildType.valueOf(b.getResult()));
				build.setDisplayName(b.getDisplayName());
				build.setName(job.getName());

				for (net.whippetcode.hudson.domain.ChangeSet c : JavaUtil.convertSeqToJList(b.getChangeSets()))
				{
					ChangeSetItem item = new ChangeSetItem();
					item.setMessage(c.getMessage());
					item.setRevision(c.getRevision());
					item.setUser(c.getUser());

					for (net.whippetcode.hudson.domain.ChangeSetPath p : JavaUtil.convertSeqToJList(c.getPaths()))
					{
						ChangeSetItemPath path = new ChangeSetItemPath();
						path.setFilePath(p.getFilePath());
						path.setEditType(EditType.valueOf(p.getEditType().toUpperCase()));

						item.addPath(path);
					}
					build.addItem(item);
				}

				buildParent.getChildren().add(build);
			}

			returnList.add(job);
		}

		return  returnList;
	}
}
