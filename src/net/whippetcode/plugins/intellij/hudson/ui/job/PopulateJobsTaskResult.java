package net.whippetcode.plugins.intellij.hudson.ui.job;

import net.whippetcode.plugins.intellij.hudson.domain.job.Job;
import net.whippetcode.plugins.intellij.hudson.domain.job.Status;

import java.io.Serializable;
import java.util.List;

/**
 * @author David A. Freels Sr.
 */
public class PopulateJobsTaskResult implements Serializable
{
	private static final long serialVersionUID = -7608562453887783905L;

	private List<Job> jobs;
	private String statusMessage;
	private Status status;

	public PopulateJobsTaskResult(List<Job> jobs, String statusMessage, Status status)
	{
		this.jobs = jobs;
		this.statusMessage = statusMessage;
		this.status = status;
	}

	public List<Job> getJobs()
	{
		return jobs;
	}

	public String getStatusMessage()
	{
		return statusMessage;
	}

	public Status getStatus()
	{
		return status;
	}
}
