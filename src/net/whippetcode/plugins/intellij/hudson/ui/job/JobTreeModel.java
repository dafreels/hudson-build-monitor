/*
 * Copyright (c) 2009, David A. Freels Sr.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that
 * the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <ORGANIZATION> nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package net.whippetcode.plugins.intellij.hudson.ui.job;

import net.whippetcode.plugins.intellij.hudson.domain.job.AbstractChildNode;
import net.whippetcode.plugins.intellij.hudson.domain.job.Build;
import net.whippetcode.plugins.intellij.hudson.domain.job.ChildGroup;
import net.whippetcode.plugins.intellij.hudson.domain.job.Job;
import net.whippetcode.plugins.intellij.hudson.domain.job.Module;
import net.whippetcode.plugins.intellij.hudson.domain.job.Workspace;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import java.util.ArrayList;
import java.util.List;

/**
 * This class provides a TreeModel that allows the Job classes to be mapped to tree nodes.
 *
 * @author David A. Freels Sr.
 */
public class JobTreeModel extends DefaultTreeModel
{
	private static final long serialVersionUID = 9072948174037827016L;

	private List<Job> jobs;

	public JobTreeModel()
	{
		super(new DefaultMutableTreeNode("Jobs"));
		this.jobs = new ArrayList<Job>();
	}

	public void clear()
	{
		jobs.clear();
		reload();
	}

	/**
	 * Set the backing list of Job objects. This method does not refresh the UI.
	 * Call reload to update the UI.
	 *
	 * @param pkgs The list of Jobs to display.
	 */
	public void setJobs(List<Job> pkgs)
	{
		this.jobs = pkgs;
	}

	@Override
	public int getChildCount(Object o)
	{
		if (o instanceof Job)
		{
			Job parent = (Job) o;
			return parent.getChildCount();
		}
		else if (o instanceof ChildGroup)
		{
			ChildGroup parent = (ChildGroup) o;
			return parent.getChildren().size();
		}
		else if (o instanceof Build || o instanceof Module)
		{
			return 0;
		}
		else if (o instanceof Workspace)
		{
			Workspace parent = (Workspace) o;
			return parent.isDirectory() ? parent.getChildren().size() : 0;
		}
		else if (o instanceof DefaultMutableTreeNode)
		{
			return jobs != null ? jobs.size() : 0;
		}

		return 0;
	}

	/**
	 * @see javax.swing.tree.TreeModel#getChild(Object,int)
	 */
	public Object getChild(Object arg0, int arg1)
	{

		if (arg0 instanceof Job)
		{
			Job parent = (Job) arg0;
			return parent.getChild(arg1);
		}
		else if (arg0 instanceof ChildGroup)
		{
			ChildGroup parent = (ChildGroup) arg0;
			return parent.getChildren().get(arg1);
		}
		else if (arg0 instanceof DefaultMutableTreeNode)
		{
			return jobs.get(arg1);
		}
		else if (arg0 instanceof Workspace)
		{
			return ((Workspace) arg0).getChildren().get(arg1);
		}

		return null;
	}

	@Override
	public boolean isLeaf(Object o)
	{
		return o instanceof Build || o instanceof Module;
	}

	@Override
	public int getIndexOfChild(Object o, Object o1)
	{
		if (o instanceof Job)
		{
			Job parent = (Job) o;
			return parent.getIndexOfChild((AbstractChildNode) o1);
		}
		else if (o instanceof ChildGroup)
		{
			ChildGroup parent = (ChildGroup) o;
			return parent.getChildren().indexOf(o1);
		}
		else if (o instanceof DefaultMutableTreeNode)
		{
			return jobs.indexOf(o1);
		}
		else if (o instanceof Workspace)
		{
			return ((Workspace) o).getChildren().indexOf(o1);
		}

		return -1;
	}
}