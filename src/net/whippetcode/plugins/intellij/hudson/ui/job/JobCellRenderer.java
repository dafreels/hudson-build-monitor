/*
 * Copyright (c) 2009, David A. Freels Sr.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that
 * the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <ORGANIZATION> nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package net.whippetcode.plugins.intellij.hudson.ui.job;

import net.whippetcode.plugins.intellij.hudson.domain.job.Build;
import net.whippetcode.plugins.intellij.hudson.domain.job.ChildGroup;
import net.whippetcode.plugins.intellij.hudson.domain.job.Job;
import net.whippetcode.plugins.intellij.hudson.domain.job.Module;
import net.whippetcode.plugins.intellij.hudson.domain.job.Workspace;
import net.whippetcode.plugins.intellij.hudson.ui.GuiUtil;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTree;
import javax.swing.tree.DefaultTreeCellRenderer;
import java.awt.Component;
import java.awt.GridLayout;
import java.util.HashMap;
import java.util.Map;

import static net.whippetcode.plugins.intellij.hudson.ui.StatusIndicator.blueIcon;
import static net.whippetcode.plugins.intellij.hudson.ui.StatusIndicator.disabledIcon;
import static net.whippetcode.plugins.intellij.hudson.ui.StatusIndicator.getStatusIcon;
import static net.whippetcode.plugins.intellij.hudson.ui.StatusIndicator.redIcon;
import static net.whippetcode.plugins.intellij.hudson.ui.StatusIndicator.yellowIcon;

/**
 * This class renders the tree nodes.
 *
 * @author David A. Freels Sr.
 */
public class JobCellRenderer extends DefaultTreeCellRenderer
{
	private Map<String, JPanel> nodes = new HashMap<String, JPanel>();
//	private ImageIcon packageIcon;
	private ImageIcon folderIcon;
	private ImageIcon textIcon;

	public JobCellRenderer()
	{
//		packageIcon = GuiUtil.createIcon("/net/whippetcode/plugins/intellij/hudson/images/package.gif");
		folderIcon = GuiUtil.createIcon("/net/whippetcode/plugins/intellij/hudson/images/folder.gif");
		textIcon = GuiUtil.createIcon("/net/whippetcode/plugins/intellij/hudson/images/text.gif");
	}

	@Override
	public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus)
	{
		ImageIcon icon = null;

		String name = null;
		if (value instanceof Job)
		{
			Job job = (Job) value;
			name = job.toString();
			icon = getStatusIcon(job.getStatus());
		}
		else if (value instanceof Build )
		{
			Build job = (Build) value;
			name = job.toString();
			switch (job.getType())
			{
				case SUCCESSFUL:
				case SUCCESS:
					icon = blueIcon;
					break;
				case FAILED:
				case FAILURE:
					icon = redIcon;
					break;
				case UNSTABLE:
					icon = yellowIcon;
					break;
				default:
					icon = disabledIcon;
			}
		}
		else if (value instanceof Module)
		{
			Module module = (Module) value;
			name = module.getName();
			icon = getStatusIcon(module.getStatus());
		}
		else if (value instanceof ChildGroup)
		{
			ChildGroup ws = (ChildGroup) value;
			name = ws.getName();
			icon = ws.getChildren().size() > 0 ? folderIcon : textIcon;
		}
		else if (value instanceof Workspace)
		{
			Workspace ws = (Workspace) value;
			name = ws.getName();
			icon = ws.isDirectory() ? folderIcon : textIcon;
		}

		if (name != null)
		{
			JPanel panel = nodes.containsKey(name) ? nodes.get(name) : createPanel(icon, name);
			if (!nodes.containsKey(name))
			{
				nodes.put(name, panel);
			}

			((JLabel) panel.getComponent(0)).setIcon(icon);
			panel.setBackground(sel ? this.getBackgroundSelectionColor() : this.getBackgroundNonSelectionColor());
			panel.setForeground(sel ? this.getTextSelectionColor() : this.getTextNonSelectionColor());
			return panel;
		}

		return super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);
	}

	private JPanel createPanel(Icon icon, String labelName)
	{
		JPanel panel = new JPanel(new GridLayout(1, 1, 0, 0));
		panel.add(new JLabel(labelName, icon, JLabel.LEFT));
		return panel;
	}
}
