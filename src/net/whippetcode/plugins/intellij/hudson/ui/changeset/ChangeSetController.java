/*
 * Copyright (c) 2009, David A. Freels Sr.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that
 * the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <ORGANIZATION> nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package net.whippetcode.plugins.intellij.hudson.ui.changeset;

import net.whippetcode.plugins.intellij.hudson.ui.swing.*;
import net.whippetcode.plugins.intellij.hudson.ui.IViewListener;
import net.whippetcode.plugins.intellij.hudson.domain.job.ChangeSetItem;
import net.whippetcode.plugins.intellij.hudson.domain.job.ChangeSetItemPath;

import javax.swing.JList;
import javax.swing.event.ListSelectionEvent;
import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.EventObject;

/**
 * This class is the controller for the ChangeSet clases.
 *
 * @author David A. Freels Jr.
 */
public class ChangeSetController implements IViewListener
{
	private ChangeSetModel model;
	private ChangeSetView view;
	private JList changeSetList;

	/**
	 * This constuctor initializes all of the fields
	 * and sets the cell renderer for changeSetList and changeSetList2.
	 */
	public ChangeSetController()
	{
		model = new ChangeSetModel();

		changeSetList = new JList(model.getChangeSetItemListModel());
		JList changeSetList2 = new JList(model.getChangeSetItemPathListModel());

		Map<Class, IListCellRenderer> renderers = new HashMap<Class, IListCellRenderer>();
		renderers.put(ChangeSetItem.class, new ChangeSetItemCellRenderer());

		DefaultListCellRenderer renderer = new DefaultListCellRenderer(renderers);

		changeSetList.setCellRenderer(renderer);

		renderers = new HashMap<Class, IListCellRenderer>();
		renderers.put(ChangeSetItemPath.class, new ChangeSetItemPathCellRenderer());

		renderer = new DefaultListCellRenderer(renderers);

		changeSetList2.setCellRenderer(renderer);

		view = new ChangeSetView(changeSetList, changeSetList2, this);
	}

	/**
	 *
	 * @return view
	 */
	public ChangeSetView getView()
	{
		return view;
	}

	/**
	 * This method takes in a list and set the changeset item with it.
	 *
	 * @param list for the setChangeSet(List<ChangeSetItem> list)
	 */
	public void loadChangeSet(List<ChangeSetItem> list)
	{
		changeSetList.clearSelection();
		model.setChangeSet(list);
	}

	public void handleEvent(EventObject event)
	{
		ListSelectionEvent e = (ListSelectionEvent) event;

		model.loadChangeSetPaths(((JList)e.getSource()).getSelectedIndex());
	}
}
