/*
 * Copyright (c) 2009, David A. Freels Sr.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that
 * the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <ORGANIZATION> nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package net.whippetcode.plugins.intellij.hudson.ui.changeset;

import net.whippetcode.plugins.intellij.hudson.domain.job.ChangeSetItem;
import net.whippetcode.plugins.intellij.hudson.domain.job.ChangeSetItemPath;
import net.whippetcode.plugins.intellij.hudson.ui.swing.DefaultListModel;

import java.util.List;


/**
 * This class hold the data for the ChangeSet classes.
 *
 * @author David A. Freels Jr.
 */
public class ChangeSetModel
{
	private DefaultListModel<ChangeSetItem> changeSetItemListModel = new DefaultListModel<ChangeSetItem>();
	private DefaultListModel<ChangeSetItemPath> changeSetItemPathListModel = new DefaultListModel<ChangeSetItemPath>();
	private List<ChangeSetItem> changeSetList;

	/**
	 *
	 * @return changeSetItemListModel
	 */
	DefaultListModel<ChangeSetItem> getChangeSetItemListModel()
	{
		return changeSetItemListModel;
	}

	/**
	 *
	 * @return changeSetItemPathListModel
	 */
	DefaultListModel<ChangeSetItemPath> getChangeSetItemPathListModel()
	{
		return changeSetItemPathListModel;
	}

	/**
	 * This method sets the changeSet List,
	 * clears changeSetItemPathListModel,
	 * and and set the backlist for changeSetItemListModel
	 *
	 * @param list to initialize changeSetList.
	 */
	void setChangeSet(List<ChangeSetItem> list)
	{
		changeSetList = list;
		changeSetItemPathListModel.clear();
		changeSetItemListModel.setBackingList(changeSetList);
	}

	/**
	 * This method clears the changeSetItemPathListModel if the index
	 * less than 0; otherwise it will set the backlist for changeSetItemPathListModel.
	 *
	 * @param index for the changeSetList.get(int index) method
	 */
	public void loadChangeSetPaths(int index)
	{
		if (index < 0)
		{
			changeSetItemPathListModel.clear();
			return;
		}
		changeSetItemPathListModel.setBackingList(changeSetList.get(index).getPaths());
	}
}
