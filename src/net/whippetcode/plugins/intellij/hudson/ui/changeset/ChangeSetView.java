/*
 * Copyright (c) 2009, David A. Freels Sr.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that
 * the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <ORGANIZATION> nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package net.whippetcode.plugins.intellij.hudson.ui.changeset;

import net.whippetcode.plugins.intellij.hudson.ui.IViewListener;

import javax.swing.*;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;
import java.awt.*;

/**
 *
 * This class is the view for the ChangeSetItem and ChangeSetItemPath classes.
 *
 * @author David A. Freels Jr.
 */
public class ChangeSetView extends JPanel implements ListSelectionListener
{
	private IViewListener listener;

	/**
	 * Change set view constructor;calls init(JList itemList, JList itemList2).
	 *
	 * @param itemList for the changeSetItems.
	 * @param itemPathList for the changeSetItemPaths.
	 * @param listener to initialize the field "listener."
	 */
	public ChangeSetView(JList itemList, JList itemPathList, IViewListener listener)
	{
		super();
		this.listener = listener;
		init(itemList, itemPathList);
	}

	/**
	 * This method adds everything to the Changeset panel
	 *
	 * @param itemList for the changeSetItems
	 * @param itemPathList for the changeSetItemPaths
	 */
	private void init(JList itemList, JList itemPathList)
	{
		super.setLayout(new BorderLayout());

		itemList.addListSelectionListener(this);
		itemList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		JScrollPane itemListScrollPane = new JScrollPane(itemList, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		JScrollPane itemListScrollPane2 = new JScrollPane(itemPathList, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		itemListScrollPane.setForeground(Color.BLUE);

		JPanel leftPanel = new JPanel(new BorderLayout());
		leftPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEmptyBorder(), "Changes"));
		leftPanel.add(itemListScrollPane);

		JPanel rightPanel = new JPanel(new BorderLayout());
		rightPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEmptyBorder(), "Files"));
		rightPanel.add(itemListScrollPane2);

		JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
		splitPane.setDividerLocation(500);
		splitPane.setResizeWeight(0.3);

		splitPane.add(leftPanel, JSplitPane.LEFT);
		splitPane.add(rightPanel, JSplitPane.RIGHT);

		super.add(splitPane, BorderLayout.CENTER);
	}

	public void valueChanged(ListSelectionEvent e)
	{
   listener.handleEvent(e);
	}
}
