/*
 * Copyright (c) 2009, David A. Freels Sr.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that
 * the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <ORGANIZATION> nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package net.whippetcode.plugins.intellij.hudson.ui.changeset;

import net.whippetcode.plugins.intellij.hudson.domain.job.ChangeSetItemPath;
import net.whippetcode.plugins.intellij.hudson.ui.GuiUtil;
import net.whippetcode.plugins.intellij.hudson.ui.swing.IListCellRenderer;

import javax.swing.*;
import java.awt.*;
import java.util.Map;
import java.util.HashMap;

/**
 * This class renders JList cells containing ChangeSetItemPath objects.
 *
 * @author David A. Freels Sr.
 */
public class ChangeSetItemPathCellRenderer implements IListCellRenderer
{
	private Map<String, JPanel> nodes = new HashMap<String, JPanel>();

	private ImageIcon editIcon;
	private ImageIcon addIcon;
	private ImageIcon deleteIcon;

	/**
	 * The constructor; initializes the ImageIcon objects.
	 */
	public ChangeSetItemPathCellRenderer()
	{
		editIcon = GuiUtil.createIcon("/net/whippetcode/plugins/intellij/hudson/images/document_edit.gif");
		addIcon = GuiUtil.createIcon("/net/whippetcode/plugins/intellij/hudson/images/document_add.gif");
		deleteIcon = GuiUtil.createIcon("/net/whippetcode/plugins/intellij/hudson/images/document_delete.gif");
	}


	public Component renderCell(Object value, boolean isSelected, boolean hasFocus)
	{
		ChangeSetItemPath path = (ChangeSetItemPath) value;
		String name = path.toString();

		ImageIcon icon = null;

		switch (path.getEditType())
		{
			case ADD:
				icon = addIcon;
				break;
			case EDIT:
				icon = editIcon;
				break;
			case DELETE:
				icon = deleteIcon;
				break;
		}


		JPanel panel;
		if (!nodes.containsKey(name))
		{
			panel = createPanel(name, icon);
			nodes.put(name, panel);
		}
		else
		{
			panel = nodes.get(name);
		}

		panel.setBackground(isSelected ? SELECTED_BACKGROUND : BACKGROUND);
		panel.setForeground(isSelected ? SELECTED_FOREGROUND : FOREGROUND);
		return panel;
	}

	/**
	 * This method creates a JPanel and adds
	 * a JLabel to it.
	 *
	 * @param name for the JLabel.
	 * @param icon for the JLabel.
	 * @return panel
	 */
	private JPanel createPanel(String name, Icon icon)
	{
		JPanel panel = new JPanel(new GridLayout(1, 1, 0, 0));
		panel.add(new JLabel(name, icon, JLabel.LEFT));
		return panel;
	}
}
