/*
 * Copyright (c) 2009, David A. Freels Sr.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that
 * the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <ORGANIZATION> nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package net.whippetcode.plugins.intellij.hudson.ui.swing;

import javax.swing.*;
import java.awt.*;

/**
 * This interface defines the method signatures required to define a list cell renderer.
 *
 * @author David A. Freels Sr.
 */
public interface IListCellRenderer
{
	public static final Color SELECTED_BACKGROUND = UIManager.getColor("List.selectionBackground");
	public static final Color SELECTED_FOREGROUND = UIManager.getColor("List.selectionForeground");
	public static final Color BACKGROUND = UIManager.getColor("List.background");
	public static final Color FOREGROUND = UIManager.getColor("List.foreground");
	
	/**
	 * This method is called to create a component that will render the passed in value.
	 *
	 * @param value			The object to render.
	 * @param isSelected true if the cell is selected.
	 * @param hasFocus	 true if the list has focus.
	 * @return A component to display the data contained in the value.
	 */
	Component renderCell(Object value, boolean isSelected, boolean hasFocus);
}
