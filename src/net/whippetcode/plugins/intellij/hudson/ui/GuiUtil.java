/*
 * Copyright (c) 2009, David A. Freels Sr.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that
 * the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <ORGANIZATION> nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package net.whippetcode.plugins.intellij.hudson.ui;

import net.whippetcode.plugins.intellij.hudson.ui.job.JobCellRenderer;
import net.whippetcode.plugins.intellij.hudson.util.LogUtil;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.logging.Level;

/**
 * Provides basic utility methods for UI related work.
 *
 * @author David A. Freels Sr.
 */
public class GuiUtil
{
	private static final String osName = System.getProperty("os.name").toLowerCase();

	public static JButton createButton(String tooltip, ImageIcon icon, ActionListener listener, String actionCommand)
	{
		JButton button = new JButton();
		button.setIcon(icon);
		button.setToolTipText(tooltip);
		button.addActionListener(listener);
		button.setMargin(new Insets(2, 2, 2, 2));
		button.setActionCommand(actionCommand);

		return button;
	}

	public static boolean isMacOS()
	{
		return osName.startsWith("mac os x");
	}

	public static ImageIcon createIcon(String url)
	{
		ImageIcon icon;
		try
		{
			Image pack = ImageIO.read(JobCellRenderer.class.getResource(url));
			icon = new ImageIcon(pack);
		}
		catch (IOException e)
		{
			//Make an empty icon if we can't load the actual icon.
			icon = new ImageIcon();
			LogUtil.log(Level.WARNING, e.getMessage(), e);
		}

		return icon;
	}
}
