/*
 * Copyright (c) 2009, David A. Freels Sr.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that
 * the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <ORGANIZATION> nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package net.whippetcode.plugins.intellij.hudson.ui.test;

import net.whippetcode.plugins.intellij.hudson.ui.GuiUtil;
import net.whippetcode.plugins.intellij.hudson.ui.IViewListener;

import javax.swing.*;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.TreeSelectionModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author David A. Freels Sr.
 */
public class HudsonTestView extends JPanel implements ActionListener, TreeSelectionListener
{
	public static final String STDERR = "STDERR";
	public static final String STDOUT = "STDOUT";

	private IViewListener listener;
	private JButton stdOutButton;
	private JButton stdErrButton;

	public HudsonTestView(JTree testTree, IViewListener listener)
	{
		this.listener = listener;

		if (listener == null)
		{
			throw new IllegalArgumentException("IViewListener can not be null!");
		}

		JScrollPane jobScrollPane = new JScrollPane(testTree, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

		setLayout(new BorderLayout());
		add(createTestJPanel(), BorderLayout.WEST);
		add(jobScrollPane, BorderLayout.CENTER);

		testTree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
		testTree.addTreeSelectionListener(this);
	}

	public void actionPerformed(ActionEvent actionEvent)
	{
		listener.handleEvent(actionEvent);
	}

	public void valueChanged(TreeSelectionEvent treeSelectionEvent)
	{
		listener.handleEvent(treeSelectionEvent);
	}

	void setStdOutEnabled(boolean enabled)
	{
		stdOutButton.setEnabled(enabled);
	}

	void setStdErrEnabled(boolean enabled)
	{
		stdErrButton.setEnabled(enabled);
	}

	private JPanel createTestJPanel()
	{
		stdOutButton = GuiUtil.createButton("View Standard Out", GuiUtil.createIcon("/net/whippetcode/plugins/intellij/hudson/images/terminal.gif"), this, STDOUT);
		stdOutButton.setEnabled(false);

		stdErrButton = GuiUtil.createButton("View Standard Error", GuiUtil.createIcon("/net/whippetcode/plugins/intellij/hudson/images/warnings-24x24.png"), this, STDERR);
		stdErrButton.setEnabled(false);

		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

		panel.add(stdOutButton);

		panel.add(stdErrButton);

		return panel;
	}
}