/*
 * Copyright (c) 2009, David A. Freels Sr.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that
 * the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <ORGANIZATION> nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package net.whippetcode.plugins.intellij.hudson.ui.test;

import net.whippetcode.plugins.intellij.hudson.domain.test.TestClass;
import net.whippetcode.plugins.intellij.hudson.domain.test.TestMethod;
import net.whippetcode.plugins.intellij.hudson.domain.test.TestPackage;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import java.util.ArrayList;
import java.util.List;

/**
 * This class provides a TreeModel that allows the TestPackage classes to be mapped to tree nodes.
 *
 * @author David A. Freels Sr.
 */
public class TestTreeModel extends DefaultTreeModel
{
	private List<TestPackage> packages;

	public TestTreeModel()
	{
		super(new DefaultMutableTreeNode("Test Packages"));
		this.packages = new ArrayList<TestPackage>();
	}

	public void clear()
	{
		packages.clear();
		reload();
	}

	public void setPackages(List<TestPackage> pkgs)
	{
		this.packages = pkgs;
		reload();
	}

	@Override
	public int getChildCount(Object o)
	{
		if (o instanceof TestPackage)
		{
			TestPackage parent = (TestPackage) o;
			return parent.getChildCount();
		}
		else if (o instanceof TestClass)
		{
			TestClass clz = (TestClass) o;
			return clz.getChildCount();
		}
		else if (o instanceof DefaultMutableTreeNode)
		{
			return packages.size();
		}

		return 0;
	}

	/**
	 * @see javax.swing.tree.TreeModel#getChild(Object,int)
	 */
	public Object getChild(Object arg0, int arg1)
	{
		if (arg0 instanceof TestPackage)
		{
			TestPackage parent = (TestPackage) arg0;
			return parent.getTestClass(arg1);
		}
		else if (arg0 instanceof TestClass)
		{
			TestClass clz = (TestClass) arg0;
			return clz.getTestMethod(arg1);
		}
		else if (arg0 instanceof DefaultMutableTreeNode)
		{
			return packages.get(arg1);
		}

		return null;
	}

	@Override
	public boolean isLeaf(Object o)
	{
		return o instanceof TestMethod;
	}

	@Override
	public int getIndexOfChild(Object o, Object o1)
	{
		if (o instanceof TestPackage)
		{
			TestPackage parent = (TestPackage) o;
			return parent.getIndexOfChild((TestClass) o1);
		}
		else if (o instanceof TestClass)
		{
			TestClass clz = (TestClass) o;
			return clz.getIndexOfChild((TestMethod) o1);
		}
		else if (o instanceof DefaultMutableTreeNode)
		{
			return packages.indexOf(o1);
		}

		return -1;
	}
}
