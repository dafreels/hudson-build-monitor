/*
 * Copyright (c) 2009, David A. Freels Sr.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that
 * the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <ORGANIZATION> nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package net.whippetcode.plugins.intellij.hudson.ui.test;

import com.intellij.execution.ui.ConsoleView;
import com.intellij.execution.ui.ConsoleViewContentType;
import net.whippetcode.plugins.intellij.hudson.domain.test.TestClass;
import net.whippetcode.plugins.intellij.hudson.domain.test.TestMethod;
import net.whippetcode.plugins.intellij.hudson.ui.IViewListener;
import net.whippetcode.plugins.intellij.hudson.util.LogUtil;

import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.event.TreeSelectionEvent;
import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.util.EventObject;
import java.util.logging.Level;

/**
 * @author David A. Freels Sr.
 */
public class HudsonTestController implements IViewListener
{

	private HudsonTestView view;
	private HudsonTestModel model;
	private ConsoleView cView;

	public HudsonTestController(ConsoleView cView)
	{
		this.cView = cView;
		JTree testTree = new JTree();
		testTree.setCellRenderer(new TestCellRenderer());
		testTree.setLargeModel(true);

		view = new HudsonTestView(testTree, this);
		model = new HudsonTestModel();
		testTree.setModel(model.getTestTreeModel());
	}

	public HudsonTestView getView()
	{
		return view;
	}

	public void loadTests(final URL testURL)
	{
		view.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		Thread t = new Thread(new Runnable()
		{
			public void run()
			{
				try
				{
					model.clearModel();
					model.loadTests(new URL(testURL.toString() + "/testReport/api/xml?depth=3"));
				}
				catch (Exception e)
				{
					LogUtil.log(Level.SEVERE, e.getMessage(), e);
				}
				finally
				{
					try
					{
						SwingUtilities.invokeAndWait(new Runnable()
						{
							public void run()
							{
								view.setCursor(Cursor.getDefaultCursor());
							}
						});
					}
					catch (InterruptedException e)
					{
						LogUtil.log(Level.SEVERE, e.getMessage(), e);
					}
					catch (InvocationTargetException e)
					{
						LogUtil.log(Level.SEVERE, e.getMessage(), e);
					}
				}
			}
		});
		t.start();
	}

	public void handleEvent(EventObject event)
	{
		if (event instanceof TreeSelectionEvent)
		{
			model.setCurrentMethod(null);
			model.setCurrentClass(null);
			Object o = ((TreeSelectionEvent) event).getPath().getLastPathComponent();
			if (o instanceof TestMethod)
			{
				model.setCurrentMethod((TestMethod) o);
			}
			else if (o instanceof TestClass)
			{
				model.setCurrentClass((TestClass) o);
			}

			view.setStdOutEnabled(model.hasStandardOut());
			view.setStdErrEnabled(model.hasStandardError());
		}
		else if (event instanceof ActionEvent)
		{
			ActionEvent ae = (ActionEvent) event;

			if (HudsonTestView.STDERR.equals(ae.getActionCommand()))
			{

				cView.clear();
				cView.print(model.getStandardError(), ConsoleViewContentType.ERROR_OUTPUT);
			}
			else if (HudsonTestView.STDOUT.equals(ae.getActionCommand()))
			{
				cView.clear();
				cView.print(model.getStandardOut(), ConsoleViewContentType.NORMAL_OUTPUT);
			}
		}
	}
}
