/*
 * Copyright (c) 2009, David A. Freels Sr.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that
 * the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <ORGANIZATION> nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package net.whippetcode.plugins.intellij.hudson.ui.test;

import net.whippetcode.plugins.intellij.hudson.domain.test.TestClass;
import net.whippetcode.plugins.intellij.hudson.domain.test.TestMethod;
import net.whippetcode.plugins.intellij.hudson.parser.TestHandler;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import java.io.IOException;
import java.net.URL;

/**
 * @author David A. Freels Sr.
 */
public class HudsonTestModel
{
	private TestMethod currentMethod;
	private TestTreeModel testTreeModel = new TestTreeModel();
	private TestClass currentClass;

	TestTreeModel getTestTreeModel()
	{
		return testTreeModel;
	}

	public boolean hasStandardOut()
	{
		return (currentMethod != null && currentMethod.hasStandardOut()) || (currentClass != null && currentClass.hasStandardOut());
	}

	public boolean hasStandardError()
	{
		return (currentMethod != null && currentMethod.hasStandardError()) || (currentClass != null && currentClass.hasStandardError());
	}

	public String getStandardError()
	{
		if (currentMethod != null)
		{
			return currentMethod.getStandardError();
		}
		else if (currentClass != null)
		{
			return currentClass.getStandardError();
		}

		return "";
	}

	public String getStandardOut()
	{
		if (currentMethod != null)
		{
			return currentMethod.getStandardOutput();
		}
		else if (currentClass != null)
		{
			return currentClass.getStandardOutput();
		}

		return "";
	}

	void setCurrentMethod(TestMethod method)
	{
		currentMethod = method;
	}

	void clearModel()
	{
		testTreeModel.clear();
	}

	void loadTests(URL testURL) throws IOException, ParserConfigurationException, XPathExpressionException, SAXException
	{
		TestHandler handler = new TestHandler(testURL);
		testTreeModel.setPackages(handler.getPackages());
	}

	public void setCurrentClass(TestClass testClass)
	{
		this.currentClass = testClass;
	}
}
