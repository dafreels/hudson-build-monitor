/*
 * Copyright (c) 2009, David A. Freels Sr.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that
 * the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <ORGANIZATION> nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package net.whippetcode.plugins.intellij.hudson.ui.test;

import net.whippetcode.plugins.intellij.hudson.domain.job.Status;
import net.whippetcode.plugins.intellij.hudson.domain.test.AbstractTest;
import net.whippetcode.plugins.intellij.hudson.domain.test.TestClass;
import net.whippetcode.plugins.intellij.hudson.domain.test.TestMethod;
import net.whippetcode.plugins.intellij.hudson.domain.test.TestPackage;
import net.whippetcode.plugins.intellij.hudson.domain.test.TestStatus;
import net.whippetcode.plugins.intellij.hudson.ui.StatusIndicator;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTree;
import javax.swing.tree.DefaultTreeCellRenderer;
import java.awt.Component;
import java.awt.GridLayout;
import java.util.HashMap;
import java.util.Map;

/**
 * This class renders the tree nodes.
 *
 * @author David A. Freels Sr.
 */
public class TestCellRenderer extends DefaultTreeCellRenderer
{
	private Map<String, JPanel> nodes = new HashMap<String, JPanel>();

	@Override
	public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus)
	{
		if (value instanceof AbstractTest)
		{
			ImageIcon icon = null;

			String name = null;
			if (value instanceof TestMethod)
			{
				TestMethod method = (TestMethod) value;
				name = method.getName();
				icon = getStatusIcon(method.getStatus());
			}
			else if (value instanceof TestClass)
			{
				TestClass clz = (TestClass) value;
				name = clz.getName();
				icon = getStatusIcon(clz.getStatus());
			}
			else if (value instanceof TestPackage)
			{
				TestPackage pkg = (TestPackage) value;
				name = pkg.getName();
				icon = getStatusIcon(pkg.getStatus());
			}

			if (name != null)
			{
				JPanel panel = nodes.containsKey(name) ? nodes.get(name) : createPanel(icon, name);
				if (!nodes.containsKey(name))
				{
					nodes.put(name, panel);
				}

				((JLabel) panel.getComponent(0)).setIcon(icon);
				panel.setBackground(sel ? this.getBackgroundSelectionColor() : this.getBackgroundNonSelectionColor());
				panel.setForeground(sel ? this.getTextSelectionColor() : this.getTextNonSelectionColor());
				return panel;
			}
		}

		return super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);
	}

	/**
	 * This method maps the TestStatus to a specific color icon.
	 * FAILED = RED
	 * PASSED = BLUE
	 * SKIPPED/UNSTABLE = YELLOW
	 * DEFAULT = GRAY
	 *
	 * @param status The TestStatus.
	 * @return An icon that is mapped to the appropriate status.
	 */
	private ImageIcon getStatusIcon(TestStatus status)
	{
		ImageIcon icon;
		switch (status)
		{
			case FAILED:
			case UNKNOWN:
				icon = StatusIndicator.getStatusIcon(Status.RED);
				break;
			case PASSED:
				icon = StatusIndicator.getStatusIcon(Status.BLUE);
				break;
			case SKIPPED:
			case UNSTABLE:
				icon = StatusIndicator.getStatusIcon(Status.YELLOW);
				break;
			default:
				icon = StatusIndicator.getStatusIcon(Status.DISABLED);
		}
		return icon;
	}

	private JPanel createPanel(Icon icon, String labelName)
	{
		JPanel panel = new JPanel(new GridLayout(1, 1, 0, 0));
		panel.add(new JLabel(labelName, icon, JLabel.LEFT));
		return panel;
	}
}