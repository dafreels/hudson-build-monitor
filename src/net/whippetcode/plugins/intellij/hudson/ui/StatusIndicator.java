/*
Copyright (c) 2009, David A. Freels Sr.
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that
the following conditions are met:

    * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
    * Neither the name of the <ORGANIZATION> nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
*/

package net.whippetcode.plugins.intellij.hudson.ui;

import com.intellij.openapi.wm.StatusBar;
import net.whippetcode.plugins.intellij.hudson.domain.job.Status;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import java.awt.Color;

/**
 * This class provides the status bar icon view.
 *
 * @author David A. Freels Sr.
 */
public class StatusIndicator extends JPanel
{
	public static ImageIcon blueIcon;
	public static ImageIcon yellowIcon;
	public static ImageIcon redIcon;
	public static ImageIcon disabledIcon;

	private JLabel iconLabel;
	private Status currentStatus = Status.DISABLED;
	private boolean triggerPopup = false;
	private JLabel label;
	private JPanel statusPanel;
	private StatusBar statusBar;

	static
	{
		blueIcon = GuiUtil.createIcon("/net/whippetcode/plugins/intellij/hudson/images/blue.gif");
		yellowIcon = GuiUtil.createIcon("/net/whippetcode/plugins/intellij/hudson/images/yellow.gif");
		redIcon = GuiUtil.createIcon("/net/whippetcode/plugins/intellij/hudson/images/red.gif");
		disabledIcon = GuiUtil.createIcon("/net/whippetcode/plugins/intellij/hudson/images/grey.gif");
	}

	public StatusIndicator(StatusBar statusBar)
	{
		this.statusBar = statusBar;
		iconLabel = new JLabel(disabledIcon);

		this.add(iconLabel);

		statusPanel = new JPanel();
		label = new JLabel();
    statusPanel.add(label);
	}

	public void updateStatus(Status status)
	{
		if (currentStatus == status)
		{
			return;
		}

		iconLabel.setIcon(getStatusIcon(status));
		currentStatus = status;
	}

	public static ImageIcon getStatusIcon(Status status)
	{
		if (status == null)
		{
			return disabledIcon;
		}

		switch (status)
		{
			case BLUE:
				return blueIcon;
			case YELLOW:
				return yellowIcon;
			case DISABLED:
				return disabledIcon;
			default:
				return redIcon;
		}
	}

	public void updateTooltip(String message)
	{
		iconLabel.setToolTipText(message);

		if (triggerPopup)
		{
			if (statusBar != null)
			{
				statusBar.fireNotificationPopup(statusPanel, Color.white);
			}
		}
		triggerPopup = false;
	}

	public void setNotificationMessage(String notificationMessage)
	{
		label.setText(notificationMessage);
		triggerPopup = notificationMessage.length() > 0;
	}
}
