/*
 * Copyright (c) 2009, David A. Freels Sr.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that
 * the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <ORGANIZATION> nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package net.whippetcode.plugins.intellij.hudson;

import javax.swing.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

/**
 * This is the bound class that supports the view.
 *
 * @author David A. Freels Sr.
 */
public class HudsonMonitorConfiguration implements ItemListener
{
	private JTextField hudsonServerAddressTextField;
	private JCheckBox useViewCheckBox;
	private JTextField viewNameTextField;
	private JPanel rootPanel;
	private JFormattedTextField refreshIntervalFormattedTextField;
	private JCheckBox authenticationRequiredCheckbox;
	private JTextField userName;
	private JPasswordField password;
	private JLabel userNameLabel;
	private JLabel passwordLabel;
	private JCheckBox debugCheckBox;
	private JCheckBox useSelfSignedCertCheckBox;
	private JCheckBox runActionOpensBrowser;
	private JTextField timeOutTextField;
	private JPanel authenticationPanel;

	public HudsonMonitorConfiguration()
	{
		useViewCheckBox.addItemListener(this);
		authenticationRequiredCheckbox.addItemListener(this);
		debugCheckBox.addItemListener(this);
		runActionOpensBrowser.addItemListener(this);
	}

	public JPanel getRootPanel()
	{
		return rootPanel;
	}

	public void itemStateChanged(ItemEvent e)
	{
		this.viewNameTextField.setEnabled(this.useViewCheckBox.isSelected());
		this.password.setEnabled(this.authenticationRequiredCheckbox.isSelected());
		this.userName.setEnabled(this.authenticationRequiredCheckbox.isSelected());
	}

	public void setData(HudsonMonitorSettings data)
	{
		hudsonServerAddressTextField.setText(data.getServerAddress());
		useViewCheckBox.setSelected(data.isUseView());
		refreshIntervalFormattedTextField.setText(data.getRefreshInterval());
		viewNameTextField.setText(data.getViewName());
		authenticationRequiredCheckbox.setSelected(data.isAuthenticationRequired());
		userName.setText(data.getUsername());
		password.setText(data.getPassword());
		debugCheckBox.setSelected(data.isDebugCheckBox());
		useSelfSignedCertCheckBox.setSelected(data.isUseSelfSignedCert());
		runActionOpensBrowser.setSelected(data.isRunActionOpensBrowser());
		if (data.getTimeOutTextField() == null || data.getTimeOutTextField().trim().equals(""))
		{
			timeOutTextField.setText("30");
		}
		else
		{
			timeOutTextField.setText(data.getTimeOutTextField());
		}
	}

	public void getData(HudsonMonitorSettings data)
	{
		data.setServerAddress(hudsonServerAddressTextField.getText());
		data.setUseView(useViewCheckBox.isSelected());
		data.setRefreshInterval(refreshIntervalFormattedTextField.getText());
		data.setViewName(viewNameTextField.getText());
		data.setAuthenticationRequired(authenticationRequiredCheckbox.isSelected());
		data.setUsername(userName.getText());
		data.setPassword(new String(password.getPassword()));
		data.setDebugCheckBox(debugCheckBox.isSelected());
		data.setUseSelfSignedCert(useSelfSignedCertCheckBox.isSelected());
		data.setRunActionOpensBrowser(runActionOpensBrowser.isSelected());
		data.setTimeOutTextField(timeOutTextField.getText());
	}

	public boolean isModified(HudsonMonitorSettings data)
	{
		if (hudsonServerAddressTextField.getText() != null ? !hudsonServerAddressTextField.getText().equals(data.getServerAddress()) : data.getServerAddress() != null)
		{
			return true;
		}
		if (useViewCheckBox.isSelected() != data.isUseView())
		{
			return true;
		}
		if (refreshIntervalFormattedTextField.getText() != null ? !refreshIntervalFormattedTextField.getText().equals(data.getRefreshInterval()) : data.getRefreshInterval() != null)
		{
			return true;
		}
		if (viewNameTextField.getText() != null ? !viewNameTextField.getText().equals(data.getViewName()) : data.getViewName() != null)
		{
			return true;
		}
		if (authenticationRequiredCheckbox.isSelected() != data.isAuthenticationRequired())
		{
			return true;
		}
		if (userName.getText() != null ? !userName.getText().equals(data.getUsername()) : data.getUsername() != null)
		{
			return true;
		}
		if (password.getPassword() != null ? !new String(password.getPassword()).equals(data.getPassword()) : data.getPassword() != null)
		{
			return true;
		}
		if (debugCheckBox.isSelected() != data.isDebugCheckBox())
		{
			return true;
		}
		if (useSelfSignedCertCheckBox.isSelected() != data.isUseSelfSignedCert())
		{
			return true;
		}
		if (runActionOpensBrowser.isSelected() != data.isRunActionOpensBrowser())
		{
			return true;
		}
		if (!(timeOutTextField.getText().equals(data.getTimeOutTextField())))
		{
			return true;
		}
		return false;
	}
}
