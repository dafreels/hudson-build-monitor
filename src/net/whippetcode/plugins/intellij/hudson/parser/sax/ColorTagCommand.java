/*
 * Copyright (c) 2009, David A. Freels Sr.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that
 * the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <ORGANIZATION> nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package net.whippetcode.plugins.intellij.hudson.parser.sax;

import net.whippetcode.plugins.intellij.hudson.domain.job.Job;
import net.whippetcode.plugins.intellij.hudson.domain.job.Status;
import net.whippetcode.plugins.intellij.hudson.util.LogUtil;

import java.util.logging.Level;

/**
 * This class sets the Status and the running state of the Job.
 *
 * @author David A. Freels Sr.
 */
public class ColorTagCommand implements ITagCommand<Job>
{
	private static final String DISABLED = "disabled";

	public void processText(String color, Job job)
	{
		if (color.indexOf("_") != -1)
		{
			job.setRunning(true);
		}
		job.setStatus(parseStatus(color));
	}

	public Status parseStatus(String color)
	{
		Status status = Status.DISABLED;

		if (color.indexOf("_") != -1)
		{
			color = color.substring(0, color.indexOf("_"));
		}

		if ("grey".equalsIgnoreCase(color) || "aborted".equalsIgnoreCase(color))
		{
			color = DISABLED;
		}

		try
		{
			status = Status.valueOf(color.toUpperCase());
		}
		catch (Exception e)
		{
			LogUtil.log(Level.WARNING, e.getMessage(), e);
		}

		return status;
	}

	public Class getSupportedClass()
	{
		return Job.class;
	}
}
