/*
 * Copyright (c) 2009, David A. Freels Sr.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that
 * the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <ORGANIZATION> nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package net.whippetcode.plugins.intellij.hudson.parser.ws;

import net.whippetcode.plugins.intellij.hudson.domain.job.Workspace;
import net.whippetcode.plugins.intellij.hudson.util.IOUtil;
import net.whippetcode.plugins.intellij.hudson.util.LogUtil;
import net.whippetcode.util.task.ITask;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

/**
 * This class will be used to populate the next level of a workspace path.
 *
 * @author David A. Freels Sr.
 */
public class PopulateWorkspaceTask implements ITask<Workspace>
{
	private static final long serialVersionUID = 9097888233355812501L;

	private String serverAddress;
	private String startPath;
	private Workspace parent;

	public PopulateWorkspaceTask(String serverAddress, String startPath, Workspace parent)
	{
		this.parent = parent;
		this.serverAddress = serverAddress;
		this.startPath = startPath;
	}

	public void execute()
	{
		//Populate the parent unless it has already been populated
		if (parent.getChildren().size() == 0)
		{
			populate(serverAddress + "/job/" + startPath, parent);
		}

		//Populate all of the children
		for (Workspace ws : parent.getChildren())
		{
			populate(serverAddress + "/job/" + startPath + "/" + ws.getName(), ws);
		}
	}

	private void populate(String url, Workspace parent)
	{
		try
		{
			List<Workspace> ws = new ArrayList<Workspace>();
			InputStream input = IOUtil.getInputStream(new URL(url.replaceAll(" ", "%20") + "/*plain*"));
			BufferedReader reader = new BufferedReader(new InputStreamReader(input));
			String line;
			Workspace space;
			while ((line = reader.readLine()) != null)
			{
				space = new Workspace();
				int index = line.indexOf("/");
				space.setDirectory(index != -1);
				space.setName(index > 0 ? line.substring(0, index) : line);
				if (!space.isDirectory())
				{
					space.setURL(new URL(url.replaceAll(" ", "%20") + "/" + space.getName()));
				}
				ws.add(space);
			}
			parent.setChildren(ws);
		}
		catch (IOException e)
		{
			if (e.getMessage() != null && e.getMessage().indexOf("response code: 403") > 0)
			{
				LogUtil.log(Level.WARNING, "User not authorized to see the work space. Please provide credentials.");
			}
			else
			{
				LogUtil.log(Level.SEVERE, "Unable to populate workspace!", e);
			}
		}
	}

	public Workspace getResult()
	{
		return parent;
	}
}
