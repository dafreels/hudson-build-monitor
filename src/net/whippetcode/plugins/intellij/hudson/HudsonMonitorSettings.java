/*
 * Copyright (c) 2009, David A. Freels Sr.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that
 * the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <ORGANIZATION> nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package net.whippetcode.plugins.intellij.hudson;

/**
 * This class is simply a bean to hold the settings.
 *
 * @author David A. Freels Sr.
 */
public class HudsonMonitorSettings
{
	private String serverAddress;
	private String viewName;
	private String refreshInterval = "10";
	private boolean useView;
	private boolean authenticationRequired;
	private String username;
	private String password;
	private boolean debugCheckBox;
	private boolean useSelfSignedCert;
	private boolean runActionOpensBrowser;
	private String timeOutTextField;

	public String getServerAddress()
	{
		return serverAddress;
	}

	public void setServerAddress(String serverAddress)
	{
		this.serverAddress = serverAddress;
	}

	public String getViewName()
	{
		return viewName;
	}

	public void setViewName(String viewName)
	{
		this.viewName = viewName;
	}

	public String getRefreshInterval()
	{
		return refreshInterval;
	}

	public void setRefreshInterval(String refreshInterval)
	{
		this.refreshInterval = refreshInterval;
	}

	public boolean isUseView()
	{
		return useView;
	}

	public void setUseView(final boolean useView)
	{
		this.useView = useView;
	}

	public boolean isAuthenticationRequired()
	{
		return authenticationRequired;
	}

	public void setAuthenticationRequired(boolean authenticationRequired)
	{
		this.authenticationRequired = authenticationRequired;
	}

	public String getUsername()
	{
		return username;
	}

	public void setUsername(String username)
	{
		this.username = username;
	}

	public String getPassword()
	{
		return password;
	}

	public void setPassword(String password)
	{
		this.password = password;
	}

	public boolean isDebugCheckBox()
	{
		return debugCheckBox;
	}

	public void setDebugCheckBox(boolean debugCheckBox)
	{
		this.debugCheckBox = debugCheckBox;
	}

	public boolean isUseSelfSignedCert()
	{
		return useSelfSignedCert;
	}

	public void setUseSelfSignedCert(boolean useSelfSignedCertCheckBox)
	{
		this.useSelfSignedCert = useSelfSignedCertCheckBox;
	}

	public boolean isRunActionOpensBrowser()
	{
		return runActionOpensBrowser;
	}

	public void setRunActionOpensBrowser(boolean runActionOpensBrowser)
	{
		this.runActionOpensBrowser = runActionOpensBrowser;
	}

	public String getTimeOutTextField()
	{
		return timeOutTextField;
	}

	public void setTimeOutTextField(String timeOutTextField)
	{
		this.timeOutTextField = timeOutTextField;
	}
}