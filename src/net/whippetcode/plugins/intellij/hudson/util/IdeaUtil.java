/*
 * Copyright (c) 2009, David A. Freels Sr.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that
 * the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <ORGANIZATION> nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package net.whippetcode.plugins.intellij.hudson.util;

import com.intellij.openapi.diff.DiffContent;
import com.intellij.openapi.diff.DiffManager;
import com.intellij.openapi.diff.SimpleContent;
import com.intellij.openapi.diff.SimpleDiffRequest;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.LocalFileSystem;
import com.intellij.openapi.vfs.VirtualFile;

import java.io.File;
import java.io.IOException;

/**
 * This class contains helper methods useful for working with IDEA.
 *
 * @author David A. Freels Sr.
 */
public class IdeaUtil
{
	public static Project PROJECT;

	public static void compareFile(File f, String filePath) throws IOException
	{
		VirtualFile localfile = LocalFileSystem.getInstance().findFileByPath(filePath);

		compareFile(f, localfile);
	}

	public static void compareFile(File f, VirtualFile localfile)
		 throws IOException
	{
		DiffContent remote = SimpleContent.fromIoFile(f, null, null);
		DiffContent local = SimpleContent.fromFile(PROJECT, localfile);

		SimpleDiffRequest diffRequest = new SimpleDiffRequest(PROJECT, "Remote File Comparison");
		diffRequest.setContents(remote, local);

		DiffManager.getInstance().getDiffTool().show(diffRequest);
	}

	/**
	 * This method will determine if the directory name matches one of the directories in the
	 * project base directory.
	 *
	 * @param dirName The directory name to match.
	 * @return true if this is a directory in the base project directory.
	 */
	public static boolean isBaseDirDirectory(String dirName)
	{
		for (VirtualFile file : PROJECT.getBaseDir().getChildren())
		{
			if (file.getName().equals(dirName))
			{
				return true;
			}
		}

		return false;
	}

	public static VirtualFile getFileByPath(String path)
	{
		VirtualFile file = null;
		String[] pathElements = path.split("/");
		VirtualFile current = PROJECT.getBaseDir();
		for (String p : pathElements)
		{
			file = findPath(current.getChildren(), p);
			if (file == null)
			{
				return null;
			}
			current = file;
		}

		return file;
	}

	public static VirtualFile findPath(VirtualFile[] files, String path)
	{
		for (VirtualFile file : files)
		{
			if (file.getName().equals(path))
			{
				return file;
			}
		}

		return null;
	}
}
