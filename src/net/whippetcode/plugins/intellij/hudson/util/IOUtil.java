/*
 * Copyright (c) 2009, David A. Freels Sr.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that
 * the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <ORGANIZATION> nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package net.whippetcode.plugins.intellij.hudson.util;

import net.whippetcode.plugins.intellij.hudson.HudsonMonitorSettings;
import sun.misc.BASE64Encoder;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Proxy;
import java.net.URL;
import java.net.URLConnection;
import java.security.cert.X509Certificate;
import java.util.logging.Level;

/**
 * This class provides utility methods for perform IO related tasks.
 *
 * @author David A. Freels Sr.
 */
public class IOUtil
{
	private static HudsonMonitorSettings settings;
	private static String userName;
	private static String password;
	private static Proxy proxy;

	public static void setProxy(Proxy proxy)
	{
		IOUtil.proxy = proxy;
	}

	public static void setHudsonSettings(HudsonMonitorSettings s)
	{
		settings = s;

		if (settings != null)
		{
			if (settings.isAuthenticationRequired())
			{
				userName = settings.getUsername();
				password = settings.getPassword();
				try
				{
					password = CipherUtil.decryptPassword(password);
				}
				catch (Exception e)
				{
					LogUtil.log(Level.WARNING, e.getMessage(), e);
				}
			}

			if (settings.isUseSelfSignedCert())
			{
				useSelfSignedCertificate();
			}
		}

		//TODO Need to allow this to be set from admin UI.
		setProxy(Proxy.NO_PROXY);
	}

	public static byte[] readBytes(URL url) throws IOException
	{
		InputStream input = getInputStream(url);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		transferBytes(input, out);

		return out.toString().getBytes("UTF-8");
	}

	/**
	 * This method will copy the bytes from one source to another.
	 *
	 * @param input The source of the bytes.
	 * @param out	 The destination.
	 * @throws IOException Thrown if a problem occurs with the IO operations.
	 */
	private static void transferBytes(InputStream input, OutputStream out)
		 throws IOException
	{
		int read;
		byte[] buffer = new byte[4096];
		while ((read = input.read(buffer)) != -1)
		{
			out.write(buffer, 0, read);
		}
		input.close();
	}

	private static void useSelfSignedCertificate()
	{
		TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager()
		{
			public X509Certificate[] getAcceptedIssuers()
			{
				return null;
			}

			public void checkClientTrusted(X509Certificate[] certs, String authType)
			{
			}

			public void checkServerTrusted(X509Certificate[] certs, String authType)
			{
			}
		}};

		try
		{
			SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

			//This allows us to use localhost
			HostnameVerifier hv = new HostnameVerifier()
			{

				public boolean verify(String s, SSLSession sslSession)
				{
					return true;
				}
			};
			HttpsURLConnection.setDefaultHostnameVerifier(hv);
		}
		catch (Exception e)
		{
			LogUtil.log(Level.WARNING, "Unable to accept self signed certificate", e);
		}
	}

	public static InputStream getInputStream(URL url) throws IOException
	{
		URLConnection connection = IOUtil.proxy == null ? url.openConnection() : url.openConnection(IOUtil.proxy);

		if (settings.getTimeOutTextField() != null && settings.getTimeOutTextField().trim().length() > 0)
		{
			connection.setConnectTimeout(Integer.parseInt(settings.getTimeOutTextField()) * 1000);
			connection.setReadTimeout(Integer.parseInt(settings.getTimeOutTextField()) * 1000);
		}
		else
		{
			connection.setConnectTimeout(30000);
			connection.setReadTimeout(30000);
		}

		if (settings != null && settings.isAuthenticationRequired())
		{
			String userPass = userName + ":" + password;
			connection.setRequestProperty("Authorization", "Basic " + new BASE64Encoder().encode(userPass.getBytes()));
		}
		return new BufferedInputStream(connection.getInputStream());
	}

	/**
	 * This method will copy the contents from a remote URL and load them into a temporary file.
	 *
	 * @param url The URL of the remote file.
	 * @return The temproary file containing the contents.
	 * @throws IOException Thrown if an exception occurs during the reading or writing of the contents.
	 */
	public static File loadRemoteFile(URL url) throws IOException
	{
		File tempFile = File.createTempFile("RemoteFileForDiff", ".tmp");
		tempFile.deleteOnExit();

		BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(tempFile, false));
		transferBytes(getInputStream(url), out);

		out.flush();
		out.close();

		return tempFile;
	}
}
