/*
 * Copyright (c) 2009, David A. Freels Sr.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that
 * the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <ORGANIZATION> nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package net.whippetcode.plugins.intellij.hudson.util;

import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

/**
 * @author David A. Freels Jr.
 */
public final class LogUtil
{
	private static final Logger LOG = Logger.getLogger(LogUtil.class.getName());

	private static int LOG_LEVEL = Level.INFO.intValue();

	public static void setLoglevel(Level level)
	{
		if (level == null)
		{
			return;
		}

		LogUtil.LOG_LEVEL = level.intValue();
	}

	@SuppressWarnings({"ThrowableResultOfMethodCallIgnored"})
	public static void log(LogRecord record)
	{
		if (LOG_LEVEL <= record.getLevel().intValue())
		{
			record.setLevel(Level.INFO);
			LOG.log(record);
		}
	}

	@SuppressWarnings({"ThrowableInstanceNeverThrown"})
	public static void log(Level level, String message, Throwable t)
	{
		if (LOG_LEVEL <= level.intValue())
		{
			LogRecord record = new LogRecord(level, message);
			record.setThrown(t);
			Exception e = new Exception();
			StackTraceElement[] st = e.getStackTrace();
			for (StackTraceElement element : st)
			{
				if (!element.getClassName().equals(LogUtil.class.getName()))
				{
					record.setSourceClassName(element.getClassName());
					record.setSourceMethodName(element.getMethodName());
					break;
				}
			}
			log(record);
		}
	}

	public static void log(Level level, String message)
	{
		if (LOG_LEVEL <= level.intValue())
		{
			log(level, message, null);
		}
	}
}