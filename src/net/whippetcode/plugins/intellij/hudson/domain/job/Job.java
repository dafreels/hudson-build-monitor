/*
 * Copyright (c) 2009, David A. Freels Sr.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that
 * the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <ORGANIZATION> nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package net.whippetcode.plugins.intellij.hudson.domain.job;

import net.whippetcode.plugins.intellij.hudson.util.LogUtil;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

/**
 * This class models a Hudson Job.
 *
 * @author David A. Freels Sr.
 */
public class Job
{
	private String name;
	private Status status;
	private boolean running;
	private boolean queued;
	private String serverAddress;
	private URL url;
	private List<AbstractChildNode> children = new ArrayList<AbstractChildNode>();

	public Job()
	{
		Workspace workspace = new Workspace();
		workspace.setName("Workspace");
		workspace.setDirectory(true);
		children.add(workspace);
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public Status getStatus()
	{
		return status;
	}

	public void setStatus(Status status)
	{
		this.status = status;
	}

	public boolean isRunning()
	{
		return running;
	}

	public void setRunning(boolean running)
	{
		this.running = running;
	}

	public boolean isQueued()
	{
		return queued;
	}

	public void setQueued(boolean queued)
	{
		this.queued = queued;
	}

	public String getServerAddress()
	{
		return serverAddress;
	}

	public void setServerAddress(String serverAddress)
	{
		this.serverAddress = serverAddress;
	}

	public URL getJobURL()
	{
		if (url == null)
		{
			try
			{

				url = new URL(this.serverAddress + "/job/" + this.name.replaceAll(" ", "%20"));
			}
			catch (MalformedURLException e)
			{
				LogUtil.log(Level.WARNING, e.getMessage(), e);
				return null;
			}
		}
		return url;
	}

	public void addChild(AbstractChildNode build)
	{
		children.add(build);
	}

	@Override
	public String toString()
	{
		return name + (running ? " (Building)" : (queued ? " (Queued)" : ""));
	}

	public int getChildCount()
	{
		return children.size();
	}

	public AbstractChildNode getChild(int index)
	{
		return children.get(index);
	}

	public int getIndexOfChild(AbstractChildNode build)
	{
		return children.indexOf(build);
	}

	public List<AbstractChildNode> getChildren()
	{
		return children;
	}
}
