/*
 * Copyright (c) 2009, David A. Freels Sr.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that
 * the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <ORGANIZATION> nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package net.whippetcode.plugins.intellij.hudson.domain.job;

import net.whippetcode.plugins.intellij.hudson.util.LogUtil;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

/**
 * This class simply represents a build node for a Hudson job.
 *
 * @author David A. Freels Sr.
 */
public class Build extends AbstractChildNode<Build>
{
	private BuildType type;
	private String revision;
	private String date;
	private String serverName;
	private URL url;
	private String displayName;
	private List<ChangeSetItem> items = new ArrayList<ChangeSetItem>();

	public BuildType getType()
	{
		return type;
	}

	public void setType(BuildType type)
	{
		this.type = type;
	}

	public String getRevision()
	{
		return revision;
	}

	public void setRevision(String revision)
	{
		this.revision = revision;
	}

	public String getDate()
	{
		return date;
	}

	public void setDate(String date)
	{
		this.date = date;
	}

	public String getServerName()
	{
		return serverName;
	}

	public void setServerName(String serverName)
	{
		this.serverName = serverName;
	}

	public URL getURL()
	{
		if (url == null && this.revision != null && this.revision.trim().length() > 0)
		{
			try
			{
				url = new URL(serverName + "/job/" + getName().replaceAll(" ", "%20") + "/" + revision);
			}
			catch (MalformedURLException e)
			{
				LogUtil.log(Level.WARNING, e.getMessage(), e);
			}
		}
		return url;
	}

	public void addItem(ChangeSetItem item)
	{
		items.add(item);
	}

	public List<ChangeSetItem> getItems()
	{
		return items;
	}

	public String getDisplayName()
	{
		return displayName;
	}

	public void setDisplayName(String displayName)
	{
		this.displayName = displayName;
	}

	public String toString()
	{
		return getDisplayName() + " : " + date;
	}

	private String getJobStateString()
	{
		switch (type)
		{
			case SUCCESSFUL:
			case SUCCESS:
				return "Successful";
			case FAILED:
			case FAILURE:
				return "Failed";
			case ABORTED:
				return "Aborted";
			case UNSTABLE:
				return "Unstable";
			default:
				return "Stable";
		}
	}
}
