/*
 * Copyright (c) 2009, David A. Freels Sr.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that
 * the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <ORGANIZATION> nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package net.whippetcode.plugins.intellij.hudson.domain.test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This class represents a class in a test report.
 *
 * @author David A. Freels Sr.
 */
public class TestClass extends AbstractTest
{
	private Map<String, TestMethod> methods = new HashMap<String, TestMethod>();
	private List<TestMethod> methodIndex = new ArrayList<TestMethod>();

	private String failCount;
	private String passCount;
	private String skipCount;
	private String packageName;
	private TestStatus status = TestStatus.UNKNOWN;
	private String standardError = "";
	private String standardOutput = "";

	public TestStatus getStatus()
	{
		return status;
	}

	public String getFailCount()
	{
		return failCount;
	}

	public String getPassCount()
	{
		return passCount;
	}

	public String getSkipCount()
	{
		return skipCount;
	}

	public void setFailCount(String failCount)
	{
		this.failCount = failCount;
	}

	public void setPassCount(String passCount)
	{
		this.passCount = passCount;
	}

	public void setSkipCount(String skipCount)
	{
		this.skipCount = skipCount;
	}

	public void addTestMethod(TestMethod method)
	{
		methods.put(method.getName(), method);
		methodIndex.add(method);
		method.setPackageName(this.packageName);
		method.setClassName(getName());
		method.setBaseUrl(getBaseUrl());
		if (status.compareTo(method.getStatus()) > 0)
		{
			status = method.getStatus();
		}
	}

	public TestMethod getTestMethod(int index)
	{
		return methodIndex.get(index);
	}

	public String getPackageName()
	{
		return packageName;
	}

	public void setPackageName(String packageName)
	{
		this.packageName = packageName;
	}

	@Override
	public String toString()
	{
		return getName();
	}

	public int getChildCount()
	{
		return methods.size();
	}

	public int getIndexOfChild(TestMethod testMethod)
	{
		return methodIndex.indexOf(testMethod);
	}

	public String getStandardError()
	{
		return standardError;
	}

	public String getStandardOutput()
	{
		return standardOutput;
	}

	public void setStandardError(String errorMessage)
	{
		this.standardError = errorMessage;
	}

	public void setStandardOutput(String outputMessage)
	{
		this.standardOutput = outputMessage;
	}

	public boolean hasStandardOut()
	{
		return this.standardOutput.trim().length() > 0;
	}

	public boolean hasStandardError()
	{
		return (this.standardError.trim().length() > 0);
	}
}
