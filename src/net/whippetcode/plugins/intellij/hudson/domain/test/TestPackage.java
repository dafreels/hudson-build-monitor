/*
 * Copyright (c) 2009, David A. Freels Sr.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that
 * the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <ORGANIZATION> nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package net.whippetcode.plugins.intellij.hudson.domain.test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This class represents a package in a test report.
 *
 * @author David A. Freels Sr.
 */
public class TestPackage extends AbstractTest
{
	private Map<String, TestClass> classes = new HashMap<String, TestClass>();
	private List<TestClass> classIndex = new ArrayList<TestClass>();

	private TestStatus status = TestStatus.UNKNOWN;

	public TestStatus getStatus()
	{
		return status;
	}

	public TestClass getTestClass(String text)
	{
		return classes.get(text);
	}

	public TestClass getTestClass(int index)
	{
		return classIndex.get(index);
	}

	public void addTestClass(TestClass clz)
	{
		classes.put(clz.getName(), clz);
		classIndex.add(clz);
		clz.setPackageName(getName());
		clz.setBaseUrl(getBaseUrl());
	}

	public void calculatePackageStatus()
	{
		for (TestClass clz : classes.values())
		{
			if (status.compareTo(clz.getStatus()) > 0)
			{
				status = clz.getStatus();
			}
		}
	}

	@Override
	public String toString()
	{
		return getName();
	}

	public int getChildCount()
	{
		return classes.size();
	}

	public int getIndexOfChild(TestClass testClass)
	{
		return classIndex.indexOf(testClass);
	}
}
