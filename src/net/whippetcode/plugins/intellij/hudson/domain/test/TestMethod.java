/*
 * Copyright (c) 2009, David A. Freels Sr.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that
 * the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <ORGANIZATION> nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package net.whippetcode.plugins.intellij.hudson.domain.test;

/**
 * @author David A. Freels Sr.
 */
public class TestMethod extends AbstractTest
{
	private String standardError = "";
	private String standardOutput = "";
	private TestStatus status = TestStatus.UNKNOWN;
	private String packageName;
	private String className;

	public String getStandardError()
	{
		return standardError;
	}

	public String getStandardOutput()
	{
		return standardOutput;
	}

	public TestStatus getStatus()
	{
		return status;
	}

	public void setStandardError(String standardError)
	{
		this.standardError = standardError;
	}

	public void setStandardOutput(String standardOutput)
	{
		this.standardOutput = standardOutput;
	}

	public void setStatus(TestStatus status)
	{
		this.status = status;
	}

	public String getPackageName()
	{
		return packageName;
	}

	public void setPackageName(String packageName)
	{
		this.packageName = packageName;
	}

	public String getClassName()
	{
		return className;
	}

	public void setClassName(String className)
	{
		this.className = className;
	}

	public String constructTestURL()
	{
		return getBaseUrl() + "/testReport/" + this.packageName + "/" + this.className + "/" + getName().replaceAll("\\.", "_") + "/api/xml";
	}

	@Override
	public String toString()
	{
		return getName();
	}

	public boolean hasStandardOut()
	{
		return this.standardOutput.trim().length() > 0;
	}

	public boolean hasStandardError()
	{
		return getBaseUrl() != null || (this.standardError.trim().length() > 0);
	}
}
