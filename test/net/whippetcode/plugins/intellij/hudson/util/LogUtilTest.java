/*
 * Copyright (c) 2009, David A. Freels Sr.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that
 * the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <ORGANIZATION> nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package net.whippetcode.plugins.intellij.hudson.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import org.junit.Test;

import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

/**
 * @author David A. Freels Sr.
 */
public class LogUtilTest
{
	@Test
	public void testLog()
	{
		Logger log = Logger.getLogger(LogUtil.class.getName());
		Testhandler testhandler = new Testhandler();
		log.addHandler(testhandler);

		LogUtil.log(Level.CONFIG, "test message");
		assertNull(testhandler.logRecord);

		LogUtil.log(Level.INFO, "info message");
		assertNotNull(testhandler.logRecord);
		assertEquals(LogUtilTest.class.getName(), testhandler.logRecord.getSourceClassName());
		assertEquals("testLog", testhandler.logRecord.getSourceMethodName());

		testhandler.logRecord = null;
		LogUtil.setLoglevel(Level.FINE);
		LogUtil.log(Level.FINEST, "Finest message");
		assertNull(testhandler.logRecord);

		LogUtil.setLoglevel(Level.FINEST);
		LogUtil.log(Level.FINEST, "Finest message");
		assertNotNull(testhandler.logRecord);
	}

	public class Testhandler extends Handler
	{
		private LogRecord logRecord;

		public void publish(LogRecord logRecord)
		{
			this.logRecord = logRecord;
		}

		public void flush()
		{
		}

		public void close() throws SecurityException
		{
		}
	}
}
